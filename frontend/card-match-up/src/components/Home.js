import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Logo_AlfabetoDaMemoria from "../Img/Logo_AlfabetoDaMemoria.png";

export class Home extends Component {
  
  render() {
    return (
      <div className="home">

          <div className="justify-center">
              <img src={Logo_AlfabetoDaMemoria} height="250" alt="Alfabeto Da Memoria"/>
              
              <p>
              Tópicos Especiais em sistemas II 
              </p>

              <p>
              <label>
                <input type="text" name="name" placeholder="Insira seu nome"/>
              </label>
              </p>
              
              <label>
                <input type="radio" value="option1" checked={true} />
                 Modo Letras 
                </label>
              
              
              <label>
                <input type="radio" value="option1" checked={false} />
                Modo Silabas
              </label>
             
              <p>
              <Link to="/game">
                  <button class="GameButtons">Iniciar Jogo</button>
              </Link>
              </p>


          </div>



      </div>
      
    );
  }
}
