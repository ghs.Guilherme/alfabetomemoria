import React, { PureComponent } from 'react';
import Header from './header/Header';
import Card from './card/Card';
import GameOver from './card/GameOver';

import '../styles/mainWeb.css';

class Game extends PureComponent {
  

  state = { 
    isFlipped: Array(6).fill(false),
    shuffledCard: Game.duplicateCard().sort(() => Math.random() - 0.5),
    clickCount: 1,
    prevSelectedCard: -1,
    prevCardId: -1
  };

/*
  A linha 25–29 contém o método para duplicar a matriz de números de cartões. 
  Isso ocorre porque cada número de cartão deve ter uma duplicata. O método duplicateCard 
  retornará uma matriz de comprimento 16 (4x4) que é [0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]. 
  Essa matriz é aleatória quando é passada para o objeto de estado shuffledCard.
  EDIT: Professora Ana solicitou 2x3...
*/
  static duplicateCard = () => {
    return [0,1,2].reduce((preValue, current, index, array) => {
      return preValue.concat([current, current])
    },[]);
  };

/*
  A linha 37 - 63 contém o método para virar o cartão quando ele é clicado para revelar o 
  número do cartão. O método altera o isFlippedstate do cartão para true e impede que um 
  cartão que já esteja invertido responda ao evento click. Na linha 55, verificamos se o 
  número de cartões invertidos é dois, para que possamos verificar se os dois cartões são iguais.
*/
  handleClick = event => {
    event.preventDefault();
    const cardId = event.target.id;
    const newFlipps = this.state.isFlipped.slice();
    this.setState({
        prevSelectedCard: this.state.shuffledCard[cardId],
        prevCardId: cardId
    });

    if (newFlipps[cardId] === false) {
      newFlipps[cardId] = !newFlipps[cardId];
      this.setState(prevState => ({ 
        isFlipped: newFlipps,
        clickCount: this.state.clickCount + 1
      }));
      

      if (this.state.clickCount === 2) {
        this.setState({ clickCount: 1 });
        const prevCardId = this.state.prevCardId;
        const newCard = this.state.shuffledCard[cardId];
        const previousCard = this.state.prevSelectedCard;

        this.isCardMatch(previousCard, newCard, prevCardId, cardId);
      }
    }
  };

  /*
    A linha 69–87 é o método que verifica se as duas cartas invertidas são iguais. 
    Este método é chamado na linha 46, como visto acima. O método setTimeout usado ao 
    definir o estado é para que o flip do cartão não seja abrupto.
  */
  isCardMatch = (card1, card2, card1Id, card2Id) => {
    if (card1 === card2) {
      const hideCard = this.state.shuffledCard.slice();
      hideCard[card1Id] = -1;
      hideCard[card2Id] = -1;
      setTimeout(() => {
        this.setState(prevState => ({
          shuffledCard: hideCard
        }))
      }, 1400);
    } else {
      const flipBack = this.state.isFlipped.slice();
      flipBack[card1Id] = false;
      flipBack[card2Id] = false;
      setTimeout(() => {
        this.setState(prevState => ({ isFlipped: flipBack }));
      }, 1400);
    }
  };

/*
  A linha 92-100 é o método restartGame. Esse método basicamente redefine o estado do jogo.
*/
  restartGame = () => {
    this.setState({
      isFlipped: Array(6).fill(false),
      shuffledCard: Game.duplicateCard().sort(() => Math.random() - 0.5),
      clickCount: 1,
      prevSelectedCard: -1,
      prevCardId: -1
    });
  };

  /*
    A linha 106–108 verifica se o jogo acabou. Se o jogo terminar, o componente GameOver será exibido.
  */
  isGameOver = () => {
    return this.state.isFlipped.every((element, index, array) => element !== false);
  };

  /*
    Linha 116–137 É o método de renderização. Na linha 120, 
    o componente Header recebe os adereços restartGame. O método isGameOver é usado para 
    renderizar o componente GameOver quando o jogo terminar, caso contrário, o componente Card
    será renderizado.
  */
  render() {
    return (
     <div>
       <Header restartGame={this.restartGame} />
       { this.isGameOver() ? <GameOver restartGame={this.restartGame} /> :
       <div className="grid-container">
          {
            this.state.shuffledCard.map((cardNumber, index) => 
              <Card
                key={index} 
                id={index} 
                cardNumber={cardNumber} 
                //cardNumberAux={1}
                isFlipped={this.state.isFlipped[index]} 
                handleClick={this.handleClick}     
              />
            )
          }
        </div>
       }
     </div>
    );
  }
}

export default Game;