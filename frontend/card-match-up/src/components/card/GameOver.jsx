import React from 'react';

const GameOver = ({ restartGame }) => (
  <div className="justify-center">
    <h1>Parabens, voce venceu!</h1>
    <h3>Selecione uma das opções acima para continuar.</h3>
    
  </div>
);

export default GameOver;