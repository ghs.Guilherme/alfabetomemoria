import React from 'react';
import { Link } from 'react-router-dom';

const Header = ({ restartGame }) => (
  <div className="grid-header-container">
    <div className="justify-left record"> <h5>Record: 0 </h5> </div>
    <div className="justify-left record"> <h5> </h5> </div>
    <div className="justify-center game-status-text"> <h5> Jogadas: 0 </h5> </div>
    <div className="justify-end">
    
    <Link to="/">
      <button class="GameButtons">Inicio</button>
    </Link>

    <button onClick={restartGame} className="GameButtons">Reiniciar Jogo</button>

    </div>
    
  </div>
);

export default Header;