import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
//import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from './components/Home';
import  Game  from './components/Game';
//
function App() {
  return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/game" component={Game} />
        </Switch>
      </BrowserRouter>
   
  );
}

export default App;
