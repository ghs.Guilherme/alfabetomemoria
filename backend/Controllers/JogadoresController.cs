using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using alfabetoMemoria.Models;

namespace alfabetoMemoria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JogadoresController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public JogadoresController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: Jogadores
        [HttpGet]
        public IActionResult Index()
        {
            List<Jogador> jogadores = _context.Jogadores.ToList();
            return Ok(jogadores);
        }

        // GET: Jogadores/4
        [HttpGet("{id}")]
        public IActionResult IndexId(int id)
        {
            try
            {
                Jogador jogador = _context.Jogadores.Where(x => x.Id == id).Single();
                return Ok(jogador);
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // POST: Jogadores/
        [HttpPost]
        public IActionResult inserirJogador([FromBody]Jogador jog)
        {
            try
            {
                _context.Jogadores.Add(jog);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }


        // DELETE: Jogadores/
        [HttpDelete]
        public IActionResult deletarJogador(int id)
        {
            try
            {
                Jogador jog;
                jog = _context.Jogadores.Find(id);
                _context.Jogadores.Remove(jog);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // EDIT: Jogadores/
        [HttpPut]
        public IActionResult editarJogador([FromBody]Jogador jog)
        {
            try
            {
                Jogador jogAux;
                jogAux = _context.Jogadores.Find(jog.Id);
                if (jog.Nome != null)
                {
                    jogAux.Nome = jog.Nome;
                }
                if (jog.RecordJogadas > 0)
                {
                    jogAux.RecordJogadas = jog.RecordJogadas;
                }
                _context.Jogadores.Update(jogAux);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }


        }
    }
}
