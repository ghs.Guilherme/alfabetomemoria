using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using alfabetoMemoria.Models;

namespace alfabetoMemoria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartasController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public CartasController(ApiDbContext context)
        {
            _context = context;
        }

        // GET: Jogadores
        [HttpGet]
        public IActionResult Index()
        {
            List<Carta> cartas = _context.Cartas.ToList();
            return Ok(cartas);
        }

        // GET: Cartas/4
        [HttpGet("{id}")]
        public IActionResult IndexId(int id)
        {
            try
            {
                Carta carta = _context.Cartas.Where(x => x.Id == id).Single();
                return Ok(carta);
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // POST: Cartas/
        [HttpPost]
        public IActionResult inserirCarta([FromBody]Carta carta)
        {
            try
            {
                _context.Cartas.Add(carta);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }


        // DELETE: Cartas/
        [HttpDelete]
        public IActionResult deletarCarta(int id)
        {
            try
            {
                Carta carta;
                carta = _context.Cartas.Find(id);
                _context.Cartas.Remove(carta);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // EDIT: Cartas/
        [HttpPut]
        public IActionResult editarCarta([FromBody]Carta carta)
        {
            try
            {
                Carta cartaAux;
                cartaAux = _context.Cartas.Find(carta.Id);
                if (carta.Nome != null)
                {
                    cartaAux.Nome = carta.Nome;
                }
                if (carta.UrlImg != null)
                {
                    cartaAux.UrlImg = carta.UrlImg;
                }
                _context.Cartas.Update(cartaAux);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }






    }

}