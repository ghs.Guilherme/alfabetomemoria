using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using alfabetoMemoria.Models;

namespace alfabetoMemoria.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JogosController : ControllerBase
    {
        private readonly ApiDbContext _context;

        public JogosController(ApiDbContext context)
        {
            _context = context;
        }


        // GET: Jogos
        [HttpGet]
        public IActionResult Index()
        {
            List<Jogo> jogos = _context.Jogos.ToList();
            return Ok(jogos);
        }

        // GET: Jogos/4
        [HttpGet("{id}")]
        public IActionResult IndexId(int id)
        {
            try
            {
                Jogo jogo = _context.Jogos.Where(x => x.Id == id).Single();
                return Ok(jogo);
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // POST: Jogos/
        [HttpPost]
        public IActionResult inserirJogo([FromBody]JogoViewModel jogo)
        {
            try
            {
                Jogo j = new Jogo();
                j.Jogadas = jogo.Jogadas;
                j.Id = jogo.Id;
                j.Jogador = _context.Jogadores.Find(jogo.JogadorId);
                


                _context.Jogos.Add(j);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception e)
                {

                    throw e;
                }
            }
            catch (System.Exception e)
            {
                return BadRequest();
                throw e;
            }

        }


        // DELETE: Jogos/
        [HttpDelete]
        public IActionResult deletarJogo(int id)
        {
            try
            {
                Jogo jogo;
                jogo = _context.Jogos.Find(id);
                _context.Jogos.Remove(jogo);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }

        }

        // EDIT: Cartas/
        [HttpPut]
        public IActionResult editarJogo([FromBody]Jogo jogo)
        {
            try
            {
                Jogo jogoAux;
                jogoAux = _context.Jogos.Find(jogo.Id);
                
				if (jogo.Jogador != null)
                {
                    jogoAux.Jogador = jogo.Jogador;
                }
				
				if (jogo.Jogadas > 0)
                {
                    jogoAux.Jogadas = jogo.Jogadas;
                }
				
				if (jogo.Cartas != null)
                {
                    jogoAux.Cartas = jogo.Cartas;
                }
             
                _context.Jogos.Update(jogoAux);

                try
                {
                    _context.SaveChanges();
                    return Ok();
                }
                catch (System.Exception)
                {

                    throw;
                }
            }
            catch (System.Exception)
            {
                return BadRequest();
                throw;
            }


        }





    }

}